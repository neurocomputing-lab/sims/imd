# Makefile for imdcode
#
# Christos Strydis, Erasmus MC, 2013 (c)



# --- MACROS
# define program name
MAIN= imdcode

# define the C compiler to use
CC= gcc #~/sims-bin/lcc #gcc

# define any compile-time flags
CFLAGS= -O3 #-std=c99 #-Wall

# define any libraries to link into executable
LIBS= #-lm

# define C source files
SRCS= imdcode.c misty1.c

# define C header files
HDRS= misty1.h

# --- TARGETS
all: ${MAIN} run

.PHONY: run
run:
	@echo #
	@echo "-- RUNNING PROGRAM --"
	./${MAIN}


#Builds the program
${MAIN}: ${SRCS} ${HDRS}
	@echo #
	@echo "-- BUILDING PROGRAM --"
	${CC} ${SRCS} ${CFLAGS} ${LIBS} -o ${MAIN}


.PHONY: clean
clean:
	@echo #
	@echo "-- CLEANING PROGRAM FILES --"
	$(RM) *.o ${MAIN} *~ core dump*

